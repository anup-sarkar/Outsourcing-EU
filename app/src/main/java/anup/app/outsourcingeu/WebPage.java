package anup.app.outsourcingeu;



import android.app.Activity;
import android.content.Context;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.DoubleBounce;


public class WebPage {
    private String url = "";
    private WebView webView;
    private   ProgressBar progressBar;
    private SwipeRefreshLayout swipeLayout;
    private  Context context;

    public WebPage( Context context,String url, WebView webView, ProgressBar progressBar, SwipeRefreshLayout swipeLayout) {
        this.url = url;
        this.webView = webView;
        this.progressBar = progressBar;
        this.swipeLayout = swipeLayout;
        this.context = context;
    }


    public void init( ) {


        DoubleBounce foldingCube = new DoubleBounce();
        progressBar.setIndeterminateDrawable(foldingCube);
        progressBar.bringToFront();


        if(!isOnline(context))
        {
            Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
            notifyMsg();
        }else {

            initWebView();



            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Insert your code here
                    if(!isOnline(context))
                    {
                        notifyMsg();
                    }
                    else
                    {
                        webView.reload();
                    }
                    // refreshes the WebView
                }
            });

        }
    }
    private void initWebView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);


        webView.loadUrl(url);



        webView.setWebChromeClient(new MyWebChromeClient(context));
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(!isOnline(context))
                {
                    webView.setVisibility(View.GONE);
                    notifyMsg();

                }
                else
                {
                    webView.setVisibility(View.VISIBLE);
                    webView.loadUrl(url);

                }

                return true;
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);
                //Toast.makeText(context, "Welcome to Hello Hi Parking!", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, "Oh no! Unknown error occurred ! " + description, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                notifyMsg();

            }


        });
        //webView.clearCache(true);
        webView.clearHistory();



    }

    public static boolean isOnline(Context ctx) {
        if (ctx == null)
            return false;

        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void notifyMsg()
    {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                context);

// Setting Dialog Title
        alertDialog2.setTitle("No Internet Connectivity !");

// Setting Dialog Message
        alertDialog2.setMessage("Please, Check you network connection");

// Setting Icon to Dialog
        alertDialog2.setIcon(android.R.drawable.ic_dialog_alert);

// Setting Positive "Yes" Btn
        alertDialog2.setPositiveButton("Reload",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog


                    }
                });

// Setting Negative "NO" Btn
        alertDialog2.setNegativeButton("Exit !",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog

                        ((Activity) context.getApplicationContext()).finish();
                    }
                });

// Showing Alert Dialog
        alertDialog2.show();
    }

    private void back() {
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }

    private void forward() {
        if (webView.canGoForward()) {
            webView.goForward();
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        Context context;

        public MyWebChromeClient(Context context) {
            super();
            this.context = context;
        }

        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            // callback.invoke(String origin, boolean allow, boolean remember);
            callback.invoke(origin, true, false);
        }


        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            android.util.Log.d("WebView", consoleMessage.message());
            return true;
        }


    }






}
