package anup.app.outsourcingeu;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.brosoft.hellohiparking.R;
import com.github.ybq.android.spinkit.style.FadingCircle;

public class SplashActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar =  findViewById(R.id.progress1);
        FadingCircle foldingCube = new FadingCircle();
        progressBar.setIndeterminateDrawable(foldingCube);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                Intent mainIntent = new Intent(getApplicationContext(),MainBottomActivity.class);
                startActivity(mainIntent);
                SplashActivity.this.finish();


            }
        }, 2000);






    }
}
