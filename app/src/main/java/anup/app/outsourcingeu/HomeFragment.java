package anup.app.outsourcingeu;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.brosoft.hellohiparking.R;

public class HomeFragment extends Fragment {

    //private String url = "https://hellohiparking.com/index.php/home/land";
    private  String url= "http://auth.outsourcingeu.com/";
    private WebView webView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeLayout;
    private Context context;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_home, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle saBundle) {
        this.webView=view.findViewById(R.id.home_webView);
        this.swipeLayout = view.findViewById(R.id.home_swipeToRefresh);
        this.progressBar=view.findViewById(R.id.home_progress);


        WebPage web=new WebPage(getContext(),url,webView,progressBar,swipeLayout);
        web.init();

    }


    }
