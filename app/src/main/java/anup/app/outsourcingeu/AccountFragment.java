package anup.app.outsourcingeu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.brosoft.hellohiparking.R;

public class AccountFragment extends Fragment {
    private String url = "https://hellohiparking.com/index.php/home/user_dashboard";
    private WebView webView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeLayout;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_account, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle saBundle) {
        this.webView=view.findViewById(R.id.acc_webView);
        this.swipeLayout = view.findViewById(R.id.acc_swipeToRefresh);
        this.progressBar=view.findViewById(R.id.acc_progress);


        WebPage web=new WebPage(getContext(),url,webView,progressBar,swipeLayout);
        web.init();

    }
}
